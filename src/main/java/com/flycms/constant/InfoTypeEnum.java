package com.flycms.constant;

public enum InfoTypeEnum {
    ALL(0, "全部", "idx*", "type*"),
    QUESTION(1, "问答", "idxquestion", "typequestion"),
    ARTICLE(2, "文章", "idxarticle", "typearticle"),
    SHARE(3, "分享", "idxshare", "typeshare");

    private Integer code;
    private String name;
    private String idxName;
    private String typeName;

    InfoTypeEnum(Integer code, String name, String idxName, String typeName){
        this.code = code;
        this.name = name;
        this.idxName = idxName;
        this.typeName = typeName;
    }

    public Integer getCode(){
        return this.code;
    }

    public String getName(){
        return this.name;
    }

    public String getIdxName(){
        return this.idxName;
    }

    public String getTypeName(){
        return this.typeName;
    }
}
