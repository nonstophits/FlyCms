package com.flycms.module.search.service;


import com.alibaba.fastjson.JSON;
import com.flycms.constant.Const;
import com.flycms.constant.InfoTypeEnum;
import com.flycms.constant.SolrConst;
import com.flycms.core.entity.PageVo;
import com.flycms.core.service.ElasticSearchService;
import com.flycms.core.utils.Md5Utils;
import com.flycms.module.article.model.Article;
import com.flycms.module.article.service.ArticleService;
import com.flycms.module.question.model.Question;
import com.flycms.module.question.service.QuestionService;
import com.flycms.module.search.model.Info;
import com.flycms.module.share.model.Share;
import com.flycms.module.share.service.ShareService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ESSearchService {
	private final Logger logger = Logger.getLogger(ESSearchService.class);
	@Autowired
	private ArticleService articleService;

    @Autowired
    private ShareService shareService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private ElasticSearchService elasticSearchService;

    @Autowired
    private SolrConst solrConst;

    private static HttpSolrClient server = null;
    //private static String url = solrConst;//其中db为自定义的core名称

    @SuppressWarnings("deprecation")
    public HttpSolrClient getServer() {
        if (server == null) {
            server = new HttpSolrClient(solrConst.getServerUrl());
            server.setDefaultMaxConnectionsPerHost(1000);
            server.setMaxTotalConnections(10000);// 最大连接数
            server.setConnectionTimeout(60000);// 设置连接超时时间（单位毫秒） 1000
            server.setSoTimeout(60000);//// 设置读数据超时时间(单位毫秒) 1000
            server.setFollowRedirects(false);// 遵循从定向
            server.setAllowCompression(true);// 允许压缩
        }
        return server;
    }

    /**
     * 索引单个文章信息
     *
     * @param id
     * @return
     * @throws ParseException
     */
    public boolean indexQuestionId(long id) {
        Question question=questionService.findQuestionById(id,2);
//        if(question!=null) {
//            question.setInfoId(question.getId());
//            String jsonString = JSON.toJSONString(question);
//            elasticSearchService.insertData(InfoTypeEnum.QUESTION.getIdxName(),InfoTypeEnum.QUESTION.getTypeName(),jsonString);
//            return true;
//        }
        return indexQuestion(question);
    }

    /**
     * 将问题添加到es索引里
     * @param question
     * @return
     */
    public boolean indexQuestion(Question question){
        if(question == null){
            return false;
        }
        //infoId与id值相同
        question.setInfoId(question.getId());
        question.setInfoType(InfoTypeEnum.QUESTION.getCode());
        String jsonString = JSON.toJSONString(question);
        elasticSearchService.insertData(InfoTypeEnum.QUESTION.getIdxName(),InfoTypeEnum.QUESTION.getTypeName(),jsonString);
        return true;
    }


    /**
     * 将所有的分享信息都索引
     *
     * @return
     * @throws ParseException
     */
    public boolean indexAllQuestion() {
        int j = questionService.getQuestionIndexCount();
        boolean res = false;
        if(j == 0) res = true;
        try {
            int c = j % 1000 == 0 ? j / 1000 : j / 1000 + 1;
            for (int i = 0; i < c; i++) {
                List<Question> list = questionService.getQuestionIndexList(i, 1000);
                list.forEach(question -> {
                    question.setInfoId(question.getId());
                    question.setInfoType(InfoTypeEnum.SHARE.getCode());
                });
                elasticSearchService.bulkInsertDataObj(InfoTypeEnum.QUESTION.getIdxName(),InfoTypeEnum.QUESTION.getTypeName(), list);
            }
            res = true;
        } catch (Exception e) {
            logger.error("索引所有问题出错", e);
        }
        return res;
    }



    /**
     * 将所有的文章信息都索引
     *
     * @return
     * @throws ParseException
     */
    public boolean indexAllArticle() throws ParseException {
        int j=articleService.getArticleIndexCount();
        boolean res = false;
        if(j == 0) res = true;
        try {
            int c = j % 1000 == 0 ? j / 1000 : j / 1000 + 1;
            for (int i = 0; i < c; i++) {
                List<Article> list = articleService.getArticleIndexList(i, 1000);
                list.forEach(article -> {
                    article.setInfoId(article.getId());
                    article.setInfoType(InfoTypeEnum.SHARE.getCode());
                });
                elasticSearchService.bulkInsertDataObj(InfoTypeEnum.ARTICLE.getIdxName(),InfoTypeEnum.ARTICLE.getTypeName(), list);
            }
            res = true;
        } catch (Exception e) {
            logger.error("索引所有文章出错", e);
        }
        return res;
    }

    /**
     * 索引单个文章信息
     *
     * @param id
     * @return
     * @throws ParseException 
     */
    public boolean indexArticleId(long id) {
        Article article=articleService.findArticleById(id,2);
        if(article!=null) {
            //infoId与id值相同
            article.setInfoId(id);
            article.setInfoType(InfoTypeEnum.ARTICLE.getCode());
            String jsonString = JSON.toJSONString(article);
            elasticSearchService.insertData(InfoTypeEnum.ARTICLE.getIdxName(),InfoTypeEnum.ARTICLE.getTypeName(),jsonString);
            return true;
        }
        return false;
    }

    /**
     * 将所有的分享信息都索引
     *
     * @return
     * @throws ParseException
     */
    public boolean indexAllShare() throws ParseException {
        int j=shareService.getShareCount(null,null,null,2);
        boolean res = false;
        if(j == 0) res = true;
        try {
            int c = j%1000==0?j/1000:j/1000+1;
            for (int i=0; i<c;i++) {
                List<Share> list = shareService.getShareIndexList(i,1000);
                list.forEach(share -> {
                    share.setInfoId(share.getId());
                    share.setInfoType(InfoTypeEnum.SHARE.getCode());
                });
                elasticSearchService.bulkInsertDataObj(InfoTypeEnum.SHARE.getIdxName(),InfoTypeEnum.SHARE.getTypeName(), list);
            }
            res = true;
        } catch (Exception e) {
            logger.error("索引所有分享出错", e);
        }
        return res;
    }

    /**
     * 索引单个文章信息
     *
     * @param id
     * @return
     * @throws ParseException
     */
    public boolean indexShareId(Long id) {
        Share share=shareService.findShareById(id,2);
        if(share!=null) {
            //infoId与id值相同
            share.setInfoId(id);
            share.setInfoType(InfoTypeEnum.SHARE.getCode());
            String jsonString = JSON.toJSONString(share);
            elasticSearchService.insertData(InfoTypeEnum.SHARE.getIdxName(),InfoTypeEnum.SHARE.getTypeName(),jsonString);
            return true;
        }
        return false;
    }

    /**
     * 根据信息类型删除索引中的数据
     * @param infoType
     *
     * @param infoId
     */
    public void indexDeleteInfo(Integer infoType,long infoId) {
        try {
//            getServer().deleteById(Md5Utils.getMD5(infoType+"-"+infoId));
//            getServer().commit();
            if(InfoTypeEnum.QUESTION.equals(infoType)){
                //问答
                logger.info("信息类型为" + InfoTypeEnum.QUESTION.getName());
                elasticSearchService.deleteData(InfoTypeEnum.QUESTION.getIdxName(), InfoTypeEnum.QUESTION.getTypeName(), ""+infoId);
            }else if(InfoTypeEnum.ARTICLE.equals(infoType)){
                //文章
                logger.info("信息类型为" + InfoTypeEnum.ARTICLE.getName());
                elasticSearchService.deleteData(InfoTypeEnum.ARTICLE.getIdxName(), InfoTypeEnum.ARTICLE.getTypeName(), ""+infoId);
            }else if(InfoTypeEnum.SHARE.equals(infoType)){
                //分享
                logger.info("信息类型为" + InfoTypeEnum.SHARE.getName());
                elasticSearchService.deleteData(InfoTypeEnum.SHARE.getIdxName(), InfoTypeEnum.SHARE.getTypeName(), ""+infoId);
            }else{
                logger.error("信息类型为" + infoType + "没有此类型的信息类型");
            }
        } catch (Exception e) {
            logger.error("从索引中删除数据失败", e);
        }
    }

    /**
     * 删除所有索引
     */
    public void deleteAllInfoindex() {
        try {
            //删除查询到的索引信息
            getServer().deleteByQuery("*");
            getServer().commit(true, true);
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据相关要求查询信息列表
     *
     * @param title
     *        信息标题
     * @param userId
     *        用户id
     * @param infoType
     *        信息类型，0是全部，1问答，2文章，3分享
     * @param page
     *        当前页码
     * @param rows
     *        每页记录数
     * @return
     * @throws IOException
     * @throws SolrServerException
     * @throws ParseException
     */
//    public PageVo<Info> searchInfo(String title, Long userId, Integer infoType, Long categoryId, String notId,String orderby,int page,int rows) throws IOException, SolrServerException, ParseException {
//        PageVo<Info> pageVo = new PageVo<Info>(page);
//        pageVo.setRows(rows);
//        if(page<1){
//            page=1;
//        }
//        SolrQuery query = new SolrQuery();
//        if(!StringUtils.isEmpty(title) || (userId!=null && userId > 0) || (infoType!=null)) {
//            // 创建组合条件串
//            StringBuilder params = new StringBuilder("");
//            // 按标题查询
//            if (!StringUtils.isEmpty(title)) {
//                String escapedkey = ClientUtils.escapeQueryChars(title);
//                params.append(" AND title:" + escapedkey + " OR content:" + escapedkey);
//            }
//
//            // 按用户id查询
//            if (userId!=null && userId > 0) {
//                params.append(" AND userId:" + userId);
//            }
//
//            // 按信息分类查询
//            if (infoType!=null) {
//                if(infoType==0){
//                    params.append(" AND infoType:*");
//                }else if(infoType==1){
//                    params.append(" AND infoType:0");
//                }else if(infoType==2){
//                    params.append(" AND infoType:1");
//                }else if(infoType==3){
//                    params.append(" AND infoType:2");
//                }
//            }
//            // 按信息分类id查询
//            if (categoryId != null && categoryId > 0) {
//                params.append(" AND categoryId:" + categoryId);
//            }
//
//            // 按用户id查询
//            if (!StringUtils.isEmpty(notId)) {
//                params.append(" AND -id:"+Md5Utils.getMD5(notId));
//            }
//            query.setQuery(this.StrStartFind(params.toString()));
//        }else {
//            query.setQuery("*:*");
//        }
//        query.setStart(pageVo.getOffset()); // 设置"起始位置"：表示从结果集的第几条数据开始显示。默认下标是0开始
//        query.setRows(pageVo.getRows()); // 设置每页显示的行数
//
//        query.set("qf","title^1");
//        query.set("fl", "*,score");  // 设置输出每条记录的索引分值
//
//        if (!StringUtils.isEmpty(title)) {
//            // 设置高亮显示———————
//            query.setHighlight(true); // 开启高亮功能
//            query.setParam("hl", "true");
//            query.setParam("hl.fl", "title");      //高亮字段
//            query.setHighlightSimplePre("<font color=\"red\">"); // 渲染标签
//            query.setHighlightSimplePost("</font>"); // 渲染标签
//            query.setHighlightSnippets(1);//结果分片数，默认为1
//            query.setHighlightFragsize(80);//每个分片的最大长度，默认为100
//        }
//        if(!StringUtils.isEmpty(orderby)){
//            if("recommend".equals(orderby)){
//                query.set("sort", "recommend desc");//按推荐值排序
//            }else if("hot".equals(orderby)){
//                query.set("sort", "weight desc");//按权重值排序
//            }
//        }else{
//            query.set("sort", "score desc,createTime desc");//排序条件
//        }
//
//        QueryResponse response = getServer().query(query);
//        SolrDocumentList dlist = response.getResults();
//        Map<String,Map<String,List<String>>> highlightMap=response.getHighlighting();
//        logger.info("文档个数：" + dlist.getNumFound());
//        //logger.info(dlist.getMaxScore());
//        // 第一个Map的键是文档的ID，第二个Map的键是高亮显示的字段名
//        List<Info> list = new ArrayList<Info>();
//        Info bean = null;
//        for (SolrDocument solrDocument : dlist) {
//            bean = new Info();
//            bean.setId(solrDocument.getFieldValue("id").toString());
//            bean.setShortUrl(solrDocument.getFieldValue("shortUrl").toString());
//            bean.setUserId(Long.parseLong(solrDocument.getFieldValue("userId").toString()));
//            bean.setTitle(solrDocument.getFieldValue("title").toString());
//            bean.setInfoId(Long.parseLong(solrDocument.getFieldValue("infoId").toString()));
//            bean.setInfoType(Integer.parseInt(solrDocument.getFieldValue("infoType").toString()));
//            bean.setContent(solrDocument.getFieldValue("content").toString());
//            String dt=solrDocument.getFieldValue("createTime").toString();
//            SimpleDateFormat sdf1= new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
//            SimpleDateFormat sdf2= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            bean.setCreateTime(sdf2.format(sdf1.parse(dt)));
//            list.add(bean);
//        }
//        pageVo.setList(list);
//        pageVo.setCount(Integer.parseInt(String.valueOf(dlist.getNumFound())));
//        return pageVo;
//    }



    /**
     * 根据相关要求查询信息列表
     *
     * @param title
     *        信息标题
     * @param userId
     *        用户id
     * @param infoType
     *        信息类型，0是全部，1问答，2文章，3分享
     * @param page
     *        当前页码
     * @param rows
     *        每页记录数
     * @return
     * @throws IOException
     * @throws SolrServerException
     * @throws ParseException
     */
    public PageVo<Info> searchInfo(String title, Long userId, Integer infoType, Long categoryId, String notId,String orderby,int page,int rows) throws Exception {
        //设置分页数据
        PageVo<Info> pageVo = new PageVo<Info>(page);
        pageVo.setInfoType(infoType);
        pageVo.setRows(rows);
        if(page<1){
            page=1;
        }

        List<Map<String, Object>> result;
        if(infoType == null){
            logger.info("没有信息类型，只查询问题列表");
            result = elasticSearchService.getDocuments(InfoTypeEnum.QUESTION.getIdxName(), InfoTypeEnum.QUESTION.getTypeName());
            //result.addAll(elasticSearchService.getDocuments(InfoTypeEnum.ARTICLE.getIdxName(), InfoTypeEnum.ARTICLE.getTypeName()));
            //result.addAll(elasticSearchService.getDocuments(InfoTypeEnum.SHARE.getIdxName(), InfoTypeEnum.SHARE.getTypeName()));

            if(ObjectUtils.isEmpty(result)){
                logger.info("没有数据");
                result = new ArrayList<Map<String, Object>>();
            }
        }else {
            if (InfoTypeEnum.ALL.equals(infoType)) {
                //全部
                logger.info("信息类型为" + InfoTypeEnum.ALL.getName());
                result = elasticSearchService.getDocuments(InfoTypeEnum.QUESTION.getIdxName(), InfoTypeEnum.QUESTION.getTypeName());
                if(ObjectUtils.isEmpty(result)){
                    //没有问答
                    result = new ArrayList<Map<String, Object>>();
                }
                result.addAll(elasticSearchService.getDocuments(InfoTypeEnum.ARTICLE.getIdxName(), InfoTypeEnum.ARTICLE.getTypeName()));
                result.addAll(elasticSearchService.getDocuments(InfoTypeEnum.SHARE.getIdxName(), InfoTypeEnum.SHARE.getTypeName()));
            } else if (InfoTypeEnum.QUESTION.equals(infoType)) {
                //问答
                logger.info("信息类型为" + InfoTypeEnum.QUESTION.getName());
                result = elasticSearchService.getDocuments(InfoTypeEnum.QUESTION.getIdxName(), InfoTypeEnum.QUESTION.getTypeName());
            } else if (InfoTypeEnum.ARTICLE.equals(infoType)) {
                //文章
                logger.info("信息类型为" + InfoTypeEnum.ARTICLE.getName());
                result = elasticSearchService.getDocuments(InfoTypeEnum.ARTICLE.getIdxName(), InfoTypeEnum.ARTICLE.getTypeName());
            } else if (InfoTypeEnum.SHARE.equals(infoType)) {
                //分享
                logger.info("信息类型为" + InfoTypeEnum.SHARE.getName());
                result = elasticSearchService.getDocuments(InfoTypeEnum.SHARE.getIdxName(), InfoTypeEnum.SHARE.getTypeName());
            } else {
                logger.error("信息类型为" + infoType + "没有此类型的信息类型");
                return pageVo;
            }
        }
        List<Info> pageVoList = new ArrayList<>();
        //map转换为info对象
        result.forEach(res -> {
            Info infoObj = new Info();
            infoObj.setInfoId((Long)res.get("infoId"));
            infoObj.setContent((String)res.get("content"));
            SimpleDateFormat sdf2= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String createTimeStr = infoObj.getCreateTime();
            Long createTimeLong = new Long(createTimeStr == null ? "0" : createTimeStr);
            infoObj.setCreateTime(sdf2.format(new Date(createTimeLong)));
            Long id = (Long)res.get("id");
            infoObj.setId(id.toString());
            infoObj.setInfoType((Integer) res.get("infoType"));
            infoObj.setShortUrl((String) res.get("shortUrl"));
            infoObj.setTitle((String) res.get("title"));
            infoObj.setUserId((Long) res.get("userId"));
            infoObj.setCategoryId((String) res.get("categoryId"));
            pageVoList.add(infoObj);
        });
        pageVo.setList(pageVoList);
        pageVo.setCount(pageVoList.size());
        return pageVo;
    }

    /**
     * Solrj查询语句拼接处理，字符串是否以AND起始，如果有则去除，没有的直接返回
     * @param str
     * @return
     */
    public static String StrStartFind(String str){
        try {
            boolean b = str.startsWith(" AND ");//判断字符串是否以‘AND’开始
            if(b) {
                return str.substring(str.indexOf(" AND ")+5,str.length());
            }return str ;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


	//企业搜索翻页处理
	public String labelPage(
			String fullName,
			String city,
			String industry,
			String scale,
			String capital, 
			int page,
			int rows,
			int maxdoc
		) throws IOException{
		
		StringBuffer link=new StringBuffer();
		if(maxdoc!=0){
            int pagesize = 10;//每页显示记录数
            if(rows>0) {
            	pagesize=rows;
            }
            int liststep = 10;//最多显示分页页数
            int pages = 1;//默认显示第一页
            if(page>1) {
            	//分页页码变量
            	pages = page;
            }
            int count = 0;
            //假设取出记录总数
            if(maxdoc>0) {
            	count = maxdoc;
            }
            int pagescount = (int) Math.ceil((double) count / pagesize);//求总页数，ceil（num）取整不小于num
            if (pagescount < pages) {
                pages = pagescount;//如果分页变量大总页数，则将分页变量设计为总页数
            }
            if (pages < 1) {
                pages = 1;//如果分页变量小于１,则将分页变量设为１
            }
            int listbegin = (pages - (int) Math.ceil((double) liststep / 2));//从第几页开始显示分页信息
            if (listbegin < 1) {
                listbegin = 1;
            }
            if(pages>=26 && pagescount>30){
            	listbegin=21;
            }
            if(pages>=26 && pagescount<30){
            	listbegin=pagescount-9;
            }
            if(pages<=pagescount && pagescount<10){
            	listbegin=1;
            }
            int listend = pages + liststep/2;//分页信息显示到第几页
            if (listend > pagescount) {
                listend = pagescount + 1;
            }
            
            if(listend<=10 && pagescount>=10){
            	listend = 11;
            }
            if(listend<10 && pagescount<10){
            	listend = pagescount + 1;
            }
            //获取搜索参数处理  
            StringBuffer buffer = new StringBuffer(); 
	        if (!StringUtils.isEmpty(fullName)) {  
	        	buffer.append("name="+java.net.URLEncoder.encode(fullName,"UTF-8")); 
	        }
	        if (!StringUtils.isEmpty(city)) {  
	        	buffer.append("&city="+city);  
	        }
	        if (!StringUtils.isEmpty(industry)) {  
	        	buffer.append("&it="+industry);  
	        }
	        if (!StringUtils.isEmpty(scale)) {  
	        	buffer.append("&sc="+scale); 
	        }
	        if (!StringUtils.isEmpty(capital)) {  
	        	buffer.append("&ct="+capital); 
	        }

            //<显示分页信息
            //<显示上一页
            if (pages > 1) {
            	link.append("<li class=\"prev\"><a href=?"+buffer+"&p=" + (pages + -1) + " rel=\"prev\">上一页</a></li>");
            }//>显示上一页
            //<显示分页码
            for (int i = listbegin; i < listend; i++) {
	            if (i != pages) {
	            	//如果i不等于当前页
	    	        if(i<=30){
	    	        	link.append("<li><a href=?"+buffer+"&p=" + i + ">" + i + "</a></li>");
	    	        }
	            } else {
	            	if(i<=30){
	            		if((listend-1)>1) {
		            		link.append("<li class=\"active\"><a href=\"javascript:void(0);\">" + i + "</a></li>");
	            		}
	            	}
	            }
            }//显示分页码>
            //<显示下一页
            if (pages != pagescount) {
	        	if(pages<30){
	        		link.append("<li class=\"next\"><a href=?"+buffer+"&p=" + (pages + 1) +" rel=\"next\">下一页</a></li>");
	        	}
            }//>显示下一页
            //>显示分页信息
		}
		return link.append("").toString();
	}

    public static void main(String[] args) {
        ESSearchService solr=new ESSearchService();
        solr.indexDeleteInfo(1,47);
        System.out.println("---");
    }
}
